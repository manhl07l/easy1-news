import { Injectable,BadRequestException,NotFoundException } from '@nestjs/common';
import { Category } from 'src/shared/entity/category.entity';
import {InjectRepository} from '@nestjs/typeorm/dist/common'
import {Repository} from 'typeorm'
import { CreateCategoryDto, UpdateCategoryDto } from 'src/shared/dto/category.dto';
import { Post } from 'src/shared/entity/post.entity';
import MeiliSearch from 'meilisearch';
import { InjectMeiliSearch } from 'nestjs-meilisearch/lib/decorators';
import { Company } from 'src/shared/entity/company.entity';

@Injectable()
export class CategoryService {
    constructor(
        @InjectRepository(Category)
        private readonly categoryRepo:Repository<Category>,
        @InjectRepository(Post)
        private readonly postRepo:Repository<Post>,
        @InjectRepository(Company)
        private readonly companyRepo:Repository<Company>,
        @InjectMeiliSearch() private readonly meiliSearch: MeiliSearch,
    ){}

    async getAllCategory(){
        const categories = await this.categoryRepo.createQueryBuilder('category')
            .leftJoinAndSelect('category.posts', 'post')
            .leftJoinAndSelect('post.companies', 'company')
            .orderBy('category.createdAt', 'DESC')
            .getMany();

        return categories;
    }

    async getOneCategory(id:number){
        const categories = await this.categoryRepo.createQueryBuilder('category')
            .where('category.id = :id', { id })
            .leftJoinAndSelect('category.posts', 'post')
            .leftJoinAndSelect('post.companies', 'company')
            .orderBy('category.createdAt', 'DESC')
            .getMany();
        
        return categories;
    }

    /* async getByCompany(id:number){
        const company = await this.companyRepo.findOne({where:{id}})
        if(!company){
            throw new NotFoundException('Company not exist')
        }
        const categories = await this.categoryRepo.createQueryBuilder('category')
            .innerJoin('category.posts', 'post')
            .innerJoin('post.companies', 'company')
            .where('company.id = :companyId', { id })
            .leftJoinAndSelect('category.posts', 'post')
            .leftJoinAndSelect('post.companies', 'company')
            .getMany();
        return categories
    } */

    async createCategory(createCategory:CreateCategoryDto){
        const category = await this.categoryRepo.findOne({where:{title:createCategory.title}})
        if(category){
            throw new BadRequestException('category exist')
        }
        return this.categoryRepo.save(createCategory)
    }

    async updateCategory(id:number,updateCategory:UpdateCategoryDto){
        const category = await this.categoryRepo.findOne({where:{id}})
        if(!category){
            throw new NotFoundException('category not exist')
        }
        return this.categoryRepo.update(id,updateCategory)
    }

    async deleteCategory(id:number){
        return this.categoryRepo.delete(id)
    }

    async findallbypaginate(limit: number, page: number){
        const [categories, count] = await this.categoryRepo
            .createQueryBuilder('category')
            .leftJoinAndSelect('category.posts', 'post')
            .leftJoinAndSelect('post.companies', 'company')
            .take(limit)
            .skip((page - 1) * limit)
            .orderBy('category.createdAt', 'DESC')
            .getManyAndCount();
        const totalPages = Math.ceil(count / limit);
        return [categories, count,totalPages];
    }

    async searchCategory(query: string){
        const posts = await this.categoryRepo.createQueryBuilder('category')
        .leftJoinAndSelect('category.posts', 'post')
        .leftJoinAndSelect('post.companies', 'company')
        .where('category.title LIKE :query', { query: `%${query}%` })
        .orderBy('category.createdAt', 'DESC')
        .getMany();

    return posts;
    }
}
