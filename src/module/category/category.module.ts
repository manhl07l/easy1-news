import { Module } from '@nestjs/common';
import { Category } from 'src/shared/entity/category.entity';
import { CategoryController } from './category.controller';
import { CategoryService } from './category.service';
import { TypeOrmModule } from '@nestjs/typeorm/dist';
import { Post } from 'src/shared/entity/post.entity';
import { Company } from 'src/shared/entity/company.entity';

@Module({
  imports:[TypeOrmModule.forFeature([Category,Post,Company])],
  controllers: [CategoryController],
  providers: [CategoryService]
})
export class CategoryModule {}
