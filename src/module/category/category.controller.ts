import { Controller,Get,Post,Delete,Put,UseGuards,Body,Param, ParseIntPipe, Query } from '@nestjs/common';
import { CategoryService } from './category.service';
import { jwtAuthGuard } from 'src/guard/jwt/jwt.guard';
import { Roles } from 'src/guard/role/role.decorations';
import { RoleGuard } from 'src/guard/role/role.guard';
import { RoleName } from 'src/shared/entity/role.entity';
import { CreateCategoryDto, UpdateCategoryDto } from 'src/shared/dto/category.dto';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';

@UseGuards(jwtAuthGuard, RoleGuard)
@ApiTags('category')
@ApiBearerAuth()
@Controller('category')
export class CategoryController {
    constructor(
        private categoryService: CategoryService
    ){}

    @Get()
    getAllCategory()
    {
        return this.categoryService.getAllCategory()
    }

    @Get('paginate')
    getpagination(@Query('limit', ParseIntPipe) limit: number, @Query('page',ParseIntPipe) page: number){
        return this.categoryService.findallbypaginate(limit,page);
    }

    @Get('search')
    searchCategory(@Query('query') query: string){
        return this.categoryService.searchCategory(query)
    }

    @Get('/:id')
    getOneCategory(@Param('id') id:string){
        return this.categoryService.getOneCategory(parseInt(id))
    }

    /* @Get('get-by-company')
    getByCompany(companyId:number){
        return this.categoryService.getByCompany(companyId)
    } */

    @Post('create-category')
    @Roles(RoleName.ADMIN,RoleName.AUTHOR)
    createCategory(@Body() createCategory:CreateCategoryDto){
        return this.categoryService.createCategory(createCategory)
    }

    @Put('update-category/:id')
    @Roles(RoleName.ADMIN,RoleName.AUTHOR)
    updateCategory(@Param('id') id: string,@Body() updateCategory:UpdateCategoryDto){
        return this.categoryService.updateCategory(parseInt(id),updateCategory)
    }

    @Delete('delete-category/:id')
    @UseGuards(RoleGuard)
    @Roles(RoleName.ADMIN,RoleName.AUTHOR)
    deleteCategory(@Param('id') id: string){
        return this.categoryService.deleteCategory(parseInt(id))
    }
}
