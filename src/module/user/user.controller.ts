import { Controller, Param, Post, Put, UseGuards,Get, Request, UseInterceptors, ClassSerializerInterceptor } from '@nestjs/common';
import { jwtAuthGuard } from 'src/guard/jwt/jwt.guard';
import { Roles } from 'src/guard/role/role.decorations';
import { changePasswordDto, CreateUserDto, UpdateUserDto } from 'src/shared/dto/user.dto';
import { RoleName } from 'src/shared/entity/role.entity';
import { UserService } from './user.service';
import { Req,Body } from '@nestjs/common';
import { RoleGuard } from 'src/guard/role/role.guard';
import { ApiBearerAuth } from '@nestjs/swagger';

@UseGuards(jwtAuthGuard)
@ApiBearerAuth()
@Controller('user')
export class UserController {
    constructor(
        private readonly userService:UserService
    ){}

    @UseInterceptors(ClassSerializerInterceptor)
    @Get('/current-user')
    getNow(@Request() req){
        const user = req.user
        return user
    }

    @UseInterceptors(ClassSerializerInterceptor)
    @Get('/get-user')
    getuser(@Req() req){
        return this.userService.getAllUser()
    }

    @Get('get-id/:id')
    getId(@Param('id') id:string){
        return this.userService.getUserById(parseInt(id))
    }
    
    @Post('/create-user')
    @UseGuards(RoleGuard)
    @Roles(RoleName.ADMIN)
    createUser(@Body() createUser:CreateUserDto){
        return this.userService.createUser(createUser)
    }

    @Put('/update-user')
    @UseGuards(RoleGuard)
    @Roles(RoleName.ADMIN)
    updateUser(@Param('id') id:number,updateUser:UpdateUserDto){
        return this.userService.updateUser(id,updateUser)
    }

    @Post('/change-password')
    @UseGuards(jwtAuthGuard)
    changePassword(@Req() req,@Body() changePassword:changePasswordDto){
        return this.userService.changePassword(req.user.id,changePassword)
    }
}
