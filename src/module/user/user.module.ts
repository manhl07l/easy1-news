import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm/dist/typeorm.module';
import { User } from 'src/shared/entity/user.entity';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { Company } from 'src/shared/entity/company.entity';
import { Role } from 'src/shared/entity/role.entity';

@Module({
  imports:[TypeOrmModule.forFeature([User,Company,Role]),],
  controllers: [UserController],
  providers: [UserService]
})
export class UserModule {
}
