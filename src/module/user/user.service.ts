import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { changePasswordDto, CreateUserDto, UpdateUserDto } from 'src/shared/dto/user.dto';
import { Company } from 'src/shared/entity/company.entity';
import { Role } from 'src/shared/entity/role.entity';
import { User } from 'src/shared/entity/user.entity';
import { Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UserService {
    constructor(
        @InjectRepository(User)
        private readonly userRepo:Repository<User>,
        @InjectRepository(Role)
        private readonly roleRepo:Repository<Role>,
        @InjectRepository(Company)
        private readonly companyRepo:Repository<Company>
    ){}

    async getAllUser(){
        return this.userRepo.createQueryBuilder('user')
        .leftJoinAndSelect('user.role', 'role')
        .getMany()
    }

    async getUserById(id:number){
        return this.userRepo.findOne({where:{id:id}})
    }

    async createUser(createUser:CreateUserDto){
        const {username,email,password,isActivated,companyName,roleName} = createUser
        const role = await this.roleRepo.findOne({where:{title:roleName}})
        const company = await this.companyRepo.findOne({where:{title:companyName}})
        const checkuser= await this.userRepo.findOne({where:{username}})
        if(checkuser){
            throw new BadRequestException('username exsit')
        }
        const checkemail= await this.userRepo.findOne({where:{email}})
        if(checkemail){
            throw new BadRequestException('email exsit')
        }
        const user = new User()
        user.username = username
        user.email = email
        user.password = password
        user.isActivated = isActivated
        user.role = role
        return this.userRepo.save(user)
    }

    async updateUser(id:number,updateUser:UpdateUserDto){
        const {username,email,password,isActivated,companyName,roleName} = updateUser
        const role = await this.roleRepo.findOne({where:{title:roleName}})
        const company = await this.companyRepo.findOne({where:{title:companyName}})
        const user= await this.userRepo.findOne({where:{id}})
        if(!user){
            throw new NotFoundException('user not exsit')
        }
        const checkuser= await this.userRepo.findOne({where:{username}})
        if(checkuser){
            throw new BadRequestException('username exsit')
        }
        const checkemail= await this.userRepo.findOne({where:{email}})
        if(checkemail){
            throw new BadRequestException('email exsit')
        }
        return this.userRepo.update(id,{username,email,password,isActivated,role})
    }

    async changePassword(id:number,changePassword:changePasswordDto){
        const {currentPassword,newPassword,confirmPassword} = changePassword
        const user = await this.userRepo.findOne({where:{id}})
        const checkCurrentPassword = await bcrypt.compare(currentPassword,user.password)
        if(!checkCurrentPassword){
            throw new BadRequestException('current password incorrect')
        }
        const checkNewPassword = await bcrypt.compare(newPassword,user.password)
        if(checkNewPassword){
            throw new BadRequestException('current password incorrect')
        }
        if(newPassword == confirmPassword){
            throw new BadRequestException('confirm password must sames newpassowrd')
        }
        const hashPassword = await bcrypt.hash(newPassword, 10)
        return this.userRepo.update(id,{password:hashPassword})
    }
}
