import { Module } from '@nestjs/common';
import { NotificationController } from './notification.controller';
import { NotificationService } from './notification.service';
import { notificationGateway } from 'src/shared/utilf/gateway/notification.gateway';

@Module({
  controllers: [NotificationController],
  providers: [NotificationService,notificationGateway]
})
export class NotificationModule {}
