import { Injectable } from '@nestjs/common';
import { notificationGateway } from 'src/shared/utilf/gateway/notification.gateway';
import * as admin from 'firebase-admin'

@Injectable()
export class NotificationService {
  constructor(private readonly webSocketGateway: notificationGateway) {}

   async sendPostCreatedNotification( token: string) {
    const message = {
      notification: {
        title: 'New Post Created',
        body: 'A new post has been created.',
      },
      token,
    };
    console.log(message)
    await admin.messaging().send(message);
  }
}