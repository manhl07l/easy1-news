import { Body, Controller, Get, Param, Post, Query } from '@nestjs/common';
import { NotificationService } from './notification.service';
import { Token } from 'meilisearch/dist/types/token';

@Controller('notification')
export class NotificationController {
    constructor(
        private notificationService: NotificationService
    ){}

    @Get('send')
    async sendNotification(@Query('token') token:string){
        await this.notificationService.sendPostCreatedNotification(token)
    }
}
