import { Controller,Get,Post,Delete,Put,UseGuards,Body,Param,Query, ParseIntPipe, Req, UseInterceptors, ClassSerializerInterceptor } from '@nestjs/common';
import { jwtAuthGuard } from 'src/guard/jwt/jwt.guard';
import { Roles } from 'src/guard/role/role.decorations';
import { RoleGuard } from 'src/guard/role/role.guard';
import { RoleName } from 'src/shared/entity/role.entity';
import { PostService } from './post.service';
import { CreatePostDto, UpdatePostDto, getPostDto } from 'src/shared/dto/post.dto';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { MediaService } from '../media/media.service';
import { query } from 'express';

@UseGuards(jwtAuthGuard,RoleGuard)
@ApiTags('post')
@ApiBearerAuth()
@Controller('post')
@UseInterceptors(ClassSerializerInterceptor)
export class PostController {
    constructor(
        private postService: PostService,
        private mediaService: MediaService
    ){}

    @Get()
    @Roles(RoleName.ADMIN,RoleName.AUTHOR)
    getAllPost(){
        return this.postService.getAllPost()
    }

    @Get('index')
    indexPost(){
        return this.postService.indexPost()
    }

    @Get('search')
    searchPost(@Query('query') query:string){
        return this.postService.searchPost(query)
    }

    /* @Get('paginate')
    getpagination(@Query('limit', ParseIntPipe) limit: number=10, @Query('page',ParseIntPipe) page: number=1){
        return this.postService.findallbypaginate(limit,page);
    } */

    @Post('multiget')
    multiget(@Body() getPostDto:getPostDto){
        return this.postService.multiget(getPostDto)
    }

    @Get('/:id')
    @Roles(RoleName.ADMIN,RoleName.AUTHOR)
    async getOnePost(@Param('id') id: string){
        return this.postService.getOnePost(parseInt(id))
    }

    @Get('index/:id')
    async indexOnePost(@Param('id') id: string){
        return this.postService.indexOnePost(parseInt(id))
    }

    /* @Get('get-by-category/:categoryId')
    getByCategory(@Param('categoryId') categoryId:string){
        return this.postService.getPostByCategory(parseInt(categoryId))
    } */

    /* @Get('get-by-date/:date')
    getByDate(@Param('date') date:string){
        return this.postService.getPostByDate(date)
    } */

    @Get('get-by-slug/:slug')
    getBySlug(@Param ('slug') slug:string,){
        return this.postService.getPostBySlug(slug)
    }

    @Post('create-post')
    @Roles(RoleName.ADMIN,RoleName.AUTHOR)
    /* @ApiConsumes('multipart/form-data')
    @ApiBody({
        schema: {
            type: 'object',
            properties: {
                file: {
                    type: 'string',
                    format: 'binary'
                },
            },
        },
    })
    @UseInterceptors(FileInterceptor('files')) */
    async createPost(@Body() createPost:CreatePostDto,@Req() req/* ,@UploadedFile() files: Express.Multer.File[] */){
        const post = await this.postService.createPost(createPost,req.user.id)
        /* await this.mediaService.uploadMultiMedia(files,req.user,post.id) */
        return post
    }

    @Put('update-post/:id')
    @Roles(RoleName.ADMIN,RoleName.AUTHOR)
    updatePost(@Param('id') id: string,@Body() updatePost:UpdatePostDto,@Req() req){
        return this.postService.updatePost(parseInt(id),updatePost,req.user.id)
    }

    @Delete('delete-post/:id')
    @Roles(RoleName.ADMIN,RoleName.AUTHOR)
    deletePost(@Param('id') id: string, @Req() req){
        return this.postService.deletePost(parseInt(id),req.user.id)
    }

    @Put('hidden-post/:id')
    @Roles(RoleName.ADMIN,RoleName.AUTHOR)
    hiddenPost(@Param('id') id: string){
        return this.postService.hiddenPost(parseInt(id))
    }
}
