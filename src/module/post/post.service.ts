import { Injectable, BadRequestException, NotFoundException,Inject, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreatePostDto, UpdatePostDto, getPostDto } from 'src/shared/dto/post.dto';
import { Category } from 'src/shared/entity/category.entity';
import { Post } from 'src/shared/entity/post.entity';
import { User } from 'src/shared/entity/user.entity';
import { In, MoreThanOrEqual, Repository } from 'typeorm';
import { MediaService } from '../media/media.service';
import { Company } from 'src/shared/entity/company.entity';
import { Vote } from 'src/shared/entity/vote.entity';
import slugify from 'slugify';

@Injectable()
export class PostService {
  constructor(
    @InjectRepository(Post)
    private readonly postRepo:Repository<Post>,
    @InjectRepository(Category)
    private readonly categoryRepo:Repository<Category>,
    @InjectRepository(User)
    private readonly userRepo:Repository<User>,
    @InjectRepository(Company)
    private readonly companyRepo:Repository<Company>,
    @Inject(MediaService)
    private readonly mediaService:MediaService,
    @InjectRepository(Vote)
    private readonly voteRepo:Repository<Vote>  ){}

  async getAllPost(): Promise<Post[]>{
    return this.postRepo.createQueryBuilder('post')
    .leftJoinAndSelect('post.categories', 'category')
    .leftJoinAndSelect('post.companies', 'company')
    .leftJoinAndSelect('post.author', 'user')
    .orderBy('post.createdAt', 'DESC')
    .getMany();
  }

  async indexPost(){
    return this.postRepo.createQueryBuilder('post')
    .leftJoinAndSelect('post.categories', 'category')
    .leftJoinAndSelect('post.companies', 'company')
    .leftJoinAndSelect('post.author', 'user')
    .where('post.status = 1')
    .orderBy('post.createdAt', 'DESC')
    .getMany();
  }

  async findallbypaginate(limit: number, page: number){
    const [posts, count] = await this.postRepo.createQueryBuilder('post')
    .leftJoinAndSelect('post.categories', 'category')
    .leftJoinAndSelect('post.companies', 'company')
    .leftJoinAndSelect('post.author', 'user')
    .take(limit)
    .skip((page - 1) * limit)
    .orderBy('post.createdAt', 'DESC')
    .getManyAndCount();
const totalPages = Math.ceil(count / limit);
return [posts, count,totalPages];
  }

  async searchPost(query: string){
    return this.postRepo.createQueryBuilder('post')
      .leftJoinAndSelect('post.categories', 'category')
      .leftJoinAndSelect('post.companies', 'company')
      .leftJoinAndSelect('post.author', 'user')
      .where('post.title LIKE :query', { query: `%${query}%` })
      .orWhere('post.description LIKE :query', { query: `%${query}%` })
      .orWhere('category.title LIKE :query', { query: `%${query}%` })
      .orWhere('company.title LIKE :query', { query: `%${query}%` })
      .orWhere('user.username LIKE :query', { query: `%${query}%` })
      .orderBy('post.createdAt', 'DESC')
      .getMany();
  }

  async getOnePost(id:number): Promise<Post>{
    return this.postRepo.createQueryBuilder('post')
    .leftJoinAndSelect('post.categories', 'category')
    .leftJoinAndSelect('post.companies', 'company')
    .leftJoinAndSelect('post.author', 'user')
    .where('post.id = :id', {id})
    .getOne()
  }

  async indexOnePost(id:number){
    const post = await this.postRepo.createQueryBuilder('post')
    .leftJoinAndSelect('post.categories', 'category')
    .leftJoinAndSelect('post.companies', 'company')
    .leftJoinAndSelect('post.author', 'user')
    .where('post.id = :id', {id})
    .getOne()
    if(post.status != 1){
      throw new BadRequestException('Post is not public')
    }
    return post
  }

  async getPostByCategory(categoryId:number): Promise<Post[]>{
    return this.postRepo
      .createQueryBuilder('post')
      .leftJoinAndSelect('post.categories', 'category')
      .leftJoinAndSelect('post.companies', 'company')
      .leftJoinAndSelect('post.author', 'user')
      .where('category.id = :categoryId', { categoryId})
      .orderBy('post.createdAt', 'DESC')
      .getMany();
  }

  async getPostByDate(createAt: string): Promise<Post[]>{
      const createdAt = new Date(createAt)
      return this.postRepo.find({where:{createdAt: MoreThanOrEqual(createdAt)}})
  }

  async getPostBySlug(slug: string): Promise<Post>{
    return this.postRepo.createQueryBuilder('post')
    .leftJoinAndSelect('post.categories', 'category')
    .leftJoinAndSelect('post.companies', 'company')
    .where('post.slug = :slug', {slug})
    .orderBy('post.createdAt', 'DESC')
    .leftJoinAndSelect('post.author', 'user')
    .getOne()
  }

  async createPost(createPost:CreatePostDto, userId:number): Promise<Post>{
    const categoriesIds = createPost.categoryIds
    const companiesIds = createPost.companiesIds
    const user = await this.userRepo.findOne({where:{id:userId}})
    const categories = await this.categoryRepo.find({where:{id:In(categoriesIds)}})
    if(!categories){
      throw new NotFoundException('category not exist')
    }
    const companies = await this.companyRepo.find({where:{id:In(companiesIds)}})
    if(!companies){
      throw new NotFoundException('company not exist')
    }
    let slug = slugify(createPost.title,{lower:true})
    const existSlug = await this.postRepo.findOne({where:{slug:slug}})
    if(existSlug){
      const random = Math.random().toString(36).substring(2, 8)
      slug =`${slug}-${random}`
    }
    const post = new Post()
      post.author = user
      post.title = createPost.title
      post.slug = slug
      post.description = createPost.description
      post.content = createPost.content
      post.categories = categories
      post.companies = companies
    const recentpost = await this.postRepo.save(post)
    for (const category of categories) {
      category.postCount++ // Increment postCount
      await this.categoryRepo.update(category.id,category)
    }
    return recentpost
  }

  async updatePost(id:number,updatePost:UpdatePostDto,userId:number){
    const categoriesIds = updatePost.categoryIds
    const companiesIds = updatePost.companiesIds
    const user = await this.userRepo.createQueryBuilder('user')
    .leftJoinAndSelect('user.role', 'role')
    .where('user.id = :userId', {userId})
    .getOne()
    const categories = await this.categoryRepo.find({where:{id:In(categoriesIds)}})
    const companies = await this.companyRepo.find({where:{id:In(companiesIds)}})
    if(!categories){
      throw new NotFoundException('post not exist')
    }
    const post = await this.getOnePost(id)
    if(post.author.id === user.id || user.role.title === 1){
    const oldCats = post.categories
      post.title = updatePost.title
      post.description = updatePost.description
      post.content = updatePost.content
      post.categories = []
      post.categories = categories
      post.status = updatePost.status
      post.companies = []
      post.companies = companies
      post.author = user
    await this.postRepo.save(post)
    for (const category of categories) {
      for(const oldCat of oldCats){
        if(category != oldCat)
        category.postCount++ // Increment postCount
        await this.categoryRepo.update(category.id,category)
      }
    }
    return post
  }
  else{throw new UnauthorizedException('Not enough permission')}
  }

  async deletePost(id:number, userId:number){
    const user = await this.userRepo.createQueryBuilder('user')
    .leftJoinAndSelect('user.role', 'role')
    .where('user.id = :userId', {userId})
    .getOne()
    const post = await this.postRepo.findOne({where:{id},relations: ['categories']})
    if(post.author.id === user.id || user.role.title === 1){
    const categories = post.categories
    for (const category of categories) {
      category.postCount--
      await this.categoryRepo.save(categories)
    }
    await this.postRepo.remove(post)}
    else{throw new UnauthorizedException('Not enough permission')}
  }

  async hiddenPost(id:number){
    const post = await this.postRepo.findOne({where:{id}})
    post.status = 0
    return this.postRepo.update(id,post)
  }

  async multiget(getPostDto: getPostDto){
    const {companyId,categoryId,page,limit,date,query}=getPostDto
    const querybuilder = this.postRepo.createQueryBuilder('post')
    .leftJoinAndSelect('post.categories', 'category')
    .leftJoinAndSelect('post.companies', 'company')
    .leftJoinAndSelect('post.author', 'user')
    .orderBy('post.createdAt', 'DESC')

    if(categoryId){
      querybuilder.where('category.id = :categoryId',{categoryId})
    }
    if(companyId){
      querybuilder.andWhere('company.id = :companyId',{companyId})
    }

    if(date){
      querybuilder.andWhere('post.createdAt >= :date', { date });
    }

    if(query){
      querybuilder.where('post.title LIKE :query', { query: `%${query}%` })
      .orWhere('post.description LIKE :query', { query: `%${query}%` })
      .orWhere('category.title LIKE :query', { query: `%${query}%` })
      .orWhere('company.title LIKE :query', { query: `%${query}%` })
      .orWhere('user.username LIKE :query', { query: `%${query}%` })
    }

    const [posts,count] = await querybuilder
    .take(limit)
    .skip((page - 1) * limit)
    .orderBy('post.createdAt', 'DESC')
    .getManyAndCount()

    const totalPages = Math.ceil(count / limit);
    return [posts, count,totalPages];
  }
}
