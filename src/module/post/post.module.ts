import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm/dist/typeorm.module';
import { Post } from 'src/shared/entity/post.entity';
import { PostController } from './post.controller';
import { PostService } from './post.service';
import { Category } from 'src/shared/entity/category.entity';
import { User } from 'src/shared/entity/user.entity';
import { MediaService } from '../media/media.service';
import { Media } from 'src/shared/entity/media.entity';
import { Company } from 'src/shared/entity/company.entity';
import { MediaModule } from '../media/media.module';
import { Vote } from 'src/shared/entity/vote.entity';
import { VoteService } from '../vote/vote.service';

@Module({
  imports: [
  TypeOrmModule.forFeature([Post,Category,User,Media,Company,Vote]),
  MediaModule],
  controllers: [PostController],
  providers: [PostService,MediaService,VoteService]
})
export class PostModule {}
