import { Injectable} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Post } from 'src/shared/entity/post.entity';
import { Repository } from 'typeorm';
import { User } from 'src/shared/entity/user.entity';
import { Comment } from 'src/shared/entity/comment.enity';
import { createCommentDto, updateCommentDto } from 'src/shared/dto/comment.dto';

@Injectable()
export class CommentService {
    constructor(
        @InjectRepository(Post)
        private readonly postRepo: Repository<Post>,
        @InjectRepository(User)
        private readonly userRepo: Repository<User>,
        @InjectRepository(Comment)
        private readonly commentRepo: Repository<Comment>,
    ){}

    async createComment(createCommentDto:createCommentDto,userId:number,postId:number){
        const user = await this.userRepo.findOne({where:{id:userId}})
        const post = await this.postRepo.findOne({where:{id:postId}})
        const comment = new Comment()
        comment.content = createCommentDto.content
        comment.user = user
        comment.post = post
        return this.commentRepo.save(comment)
    }

    async getCommentbyPost(postId: number){
        const post = await this.postRepo.findOne({where:{id:postId}})
        if (!post) {
            throw new Error(`Category with ID ${postId} not found`);
          }
        return this.commentRepo.createQueryBuilder('comment')
        .leftJoinAndSelect('comment.user', 'user')
        .leftJoinAndSelect('comment.post', 'post')
        .where('post.id = :postId',{postId})
        .getMany()
    }

    async updateComment(id:number,updateCommentDto:updateCommentDto){
        return this.commentRepo.update(id,updateCommentDto)
    }

    async deleteComment(id:number){
        return this.commentRepo.delete(id)
    }
}
