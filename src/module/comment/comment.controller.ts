import { Controller,UseGuards,Get,Param, Post,Body, Delete,Req } from '@nestjs/common';
import { jwtAuthGuard } from 'src/guard/jwt/jwt.guard';
import { CommentService } from './comment.service';
import { createCommentDto, updateCommentDto } from 'src/shared/dto/comment.dto';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';

@UseGuards(jwtAuthGuard)
@ApiTags('comment')
@ApiBearerAuth()
@Controller('comment')
export class CommentController {
    constructor(
        private commentService: CommentService
    ){}

    @Get(':id')
    getOneComment(@Param('id') id:string){
        return this.commentService.getCommentbyPost(parseInt(id))
    }

    @Post(':id/create-comment')
    createComment(@Param('id') id:string, @Body() body:createCommentDto, @Req() req){
        return this.commentService.createComment(body,req.user.id,parseInt(id))
    }

    @Post(':id/update-comment')
    updateComment(@Param('id') id:string, @Body() body:updateCommentDto){
        return this.commentService.updateComment(parseInt(id),body)
    }

    @Delete(':id')
    deleteComment(@Param('id') id:string){
        return this.commentService.deleteComment(parseInt(id))
    }
}
