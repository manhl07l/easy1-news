import { Module} from '@nestjs/common';
import { Post } from 'src/shared/entity/post.entity';
import { User } from 'src/shared/entity/user.entity';
import { CommentService } from './comment.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Comment } from 'src/shared/entity/comment.enity';
import { CommentController } from './comment.controller';

@Module({
    imports:[TypeOrmModule.forFeature([User,Post,Comment])],
    controllers: [CommentController],
    providers: [CommentService]})
export class CommentModule {}
