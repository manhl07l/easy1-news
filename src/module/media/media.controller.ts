import { Controller, Post, UploadedFile, UseInterceptors, Param, Delete, Get, Req , Query , ParseIntPipe, Body } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { MediaService } from './media.service';
import { Roles } from 'src/guard/role/role.decorations';
import { RoleName } from 'src/shared/entity/role.entity';
import { UseGuards } from '@nestjs/common';
import { RoleGuard } from 'src/guard/role/role.guard';
import { jwtAuthGuard } from 'src/guard/jwt/jwt.guard';
import { ApiTags,ApiConsumes, ApiBearerAuth, ApiBody } from '@nestjs/swagger';

@UseGuards(jwtAuthGuard, RoleGuard)
@ApiTags('media')
@ApiBearerAuth()
@Controller('media')
export class MediaController {
  constructor(private readonly mediaService: MediaService) {}

  @Get()
  getAllMedia(){
    return this.mediaService.getallMedia()
  }

  @Get('search')
  searchMedia(@Query('query') query: string){
      return this.mediaService.searchMedia(query)
  }

  @Get('paginate')
  getpagination(@Query('limit',ParseIntPipe) limit: number=10, @Query('page',ParseIntPipe) page: number=1){
    return this.mediaService.findallbypaginate(limit,page);
  }

  @Get(':id')
  getOneMedia(@Param('id') id:string){
    return this.mediaService.getOneMedia(parseInt(id))
  }

  @Get('get-by-date')
  @Roles(RoleName.ADMIN,RoleName.AUTHOR)
  getByDate(date:Date){
    return this.mediaService.getMediaByDate(date)
  }

  @Get('getByPath/:path')
  @Roles(RoleName.ADMIN,RoleName.AUTHOR)
  getByPath(@Param('path') path:string){
    return this.mediaService.getByPath(path)
  }

  @Post('update')
  @Roles(RoleName.ADMIN,RoleName.AUTHOR)
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        file: {
          type: 'string',
          format: 'binary',
        },
      },
    },
  })
  @UseInterceptors(FileInterceptor('file'))
  updateMedia(@Param('id') id:string,@UploadedFile() file: Express.Multer.File) {
    return this.mediaService.updateMedia(parseInt(id),file,);
  }

  @Delete('delete')
  @Roles(RoleName.ADMIN,RoleName.AUTHOR)
  deleteMedia(@Param('id') id:string){
    return this.mediaService.deleteMedia(parseInt(id))
  }

  @Post('upload')
  @Roles(RoleName.ADMIN,RoleName.AUTHOR)
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        file: {
          type: 'string',
          format: 'binary',
        },
        id:{
          type: 'number'
        }
      },
    },
  })
  @UseInterceptors(FileInterceptor('file'))
  async uploadMedia(@UploadedFile() file: Express.Multer.File , @Req() req, @Body('id') id: number ){
    return this.mediaService.uploadMedia(file,req.user,id)
  }
}
