import { Module } from '@nestjs/common';
import { MulterModule } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { MediaController } from './media.controller';
import { MediaService } from './media.service';
import { extname, join } from 'path';
import { TypeOrmModule } from '@nestjs/typeorm/dist';
import { Media } from 'src/shared/entity/media.entity';
import { Post } from 'src/shared/entity/post.entity';

@Module({
  imports: [
    MulterModule.register({
      storage: diskStorage({
        destination: join(__dirname, '..', '..', '..', 'uploads', 'media', 'orig'),
        filename: (req, file, cb) => {
          const randomName = Array(16)
          .fill(null)
          .map(() => Math.round(Math.random() * 16).toString(16))
          .join('');
        const timestamp = Date.now().toString();
        const extension = extname(file.originalname);
        cb(null, `${randomName}${timestamp}${extension}`);
        },
      }),
      limits: {
        fieldSize: 1024 *1024
      }
    }),
    TypeOrmModule.forFeature([Media,Post])
  ],
  controllers: [MediaController],
  providers: [MediaService],
  exports:[MediaService,MediaModule]
})
export class MediaModule {}
