import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository,MoreThanOrEqual } from 'typeorm';
import { Media } from 'src/shared/entity/media.entity';
import * as sharp from 'sharp';
import * as fs from 'fs'
import { User } from 'src/shared/entity/user.entity';
import { MediaStatus } from 'src/shared/entity/media.entity';
import MeiliSearch from 'meilisearch';
import { InjectMeiliSearch } from 'nestjs-meilisearch/lib/decorators';
import { Post } from 'src/shared/entity/post.entity';

@Injectable()
export class MediaService {
  constructor(
    @InjectRepository(Media)
    private readonly mediaRepo: Repository<Media>,
    @InjectRepository(Post)
    private readonly postRepo: Repository<Post>,
    @InjectMeiliSearch() private readonly meiliSearch: MeiliSearch,
  ) {}

  async uploadMedia(file: Express.Multer.File, user: User, id:number){
    const path = require('path');
    const media = new Media()
    media.fileName = file.filename;
    media.totalSize = file.size;
    const largePath = path.join(__dirname,'..', '..', '..', 'uploads','media','large', file.filename);
    media.largePath = path.relative(__dirname, largePath)
    const mediumPath = path.join(__dirname, '..', '..','..', 'uploads','media','medium', file.filename);
    media.mediumPath = path.relative(__dirname,mediumPath)
    const smallPath = path.join(__dirname, '..', '..','..', 'uploads','media','small', file.filename);
    media.smallPath = path.relative(__dirname,smallPath)
    const thumbnailPath = path.join(__dirname, '..', '..','..', 'uploads','media','thumbnail', file.filename);
    media.thumbnailPath = path.relative(__dirname,thumbnailPath)
    media.status = MediaStatus.PUBLISHED
    media.uploadedBy = user
    const image = sharp(file.path);
    await Promise.all([
      image
        .resize({ width: 1920, height: 1080, fit: 'inside' })
        .toFile(largePath),
      image
        .resize({ width: 960, height: 540, fit: 'inside' })
        .toFile(mediumPath),
      image
        .resize({ width: 480, height: 270, fit: 'inside' })
        .toFile(smallPath),
      image
        .resize({ width: 240, height: 135, fit: 'inside' })
        .toFile(thumbnailPath),
    ]);

    const uploadPath = path.join(__dirname, '..', '..','..', 'uploads','media','orig', file.filename);
    fs.createReadStream(file.path).pipe(fs.createWriteStream(uploadPath));
    await this.mediaRepo.save(media);
    const post = await this.postRepo.findOne({where:{id}})
    post.medias = media
    await this.postRepo.update(post.id,post)
    return media
  }

  async getallMedia(){
    const medias = await this.mediaRepo.createQueryBuilder('media')
    .leftJoinAndSelect('media.posts', 'post')
    .orderBy('media.createdAt', 'DESC')
    .getMany()
    return medias
  }

  async getOneMedia(id:number){
    const media = await this.mediaRepo.createQueryBuilder('media')
    .leftJoinAndSelect('media.posts', 'post')
    .where('media.id = :id', { id })
    .getOne()
    return media
  }

  async uploadMultiMedia(files: Express.Multer.File[], user: User, id:number){
    for(const file of files){
      const media = await this.uploadMedia(file,user,id)
      return media
    }
  }

  async getMediaByDate(createAt: Date){
    return this.mediaRepo.find({where:{createdAt: MoreThanOrEqual(createAt)}})
  }

  async getByPath(path: string){
    const detailedPath = `C:\\Users\\pc\\Desktop\\easyone-ver2\\easy1-news\\uploads\\${path}`
    const media = await this.mediaRepo.createQueryBuilder('media')
      .where('media.largePath = :detailedPath', { detailedPath: detailedPath} )
      .orWhere('media.mediumPath = :detailedPath', { detailedPath: detailedPath})
      .orWhere('media.smallPath = :detailedPath', { detailedPath: detailedPath})
      .orWhere('media.thumbnailPath = :detailedPath', { detailedPath: detailedPath})
      .getOne()

    return {detailedPath,media}
  }

  async updateMedia(id:number,file: Express.Multer.File){
    const path = require('path');
    const media = await this.mediaRepo.findOne({where:{id}})
    media.fileName = file.filename;
    media.totalSize = file.size;
    const largePath = path.join(__dirname,'..', '..', '..', 'uploads','media','large', file.filename);
    media.largePath = path.relative(__dirname, largePath)
    const mediumPath = path.join(__dirname, '..', '..','..', 'uploads','media','medium', file.filename);
    media.mediumPath = path.relative(__dirname,mediumPath)
    const smallPath = path.join(__dirname, '..', '..','..', 'uploads','media','small', file.filename);
    media.smallPath = path.relative(__dirname,smallPath)
    const thumbnailPath = path.join(__dirname, '..', '..','..', 'uploads','media','thumbnail', file.filename);
    media.thumbnailPath = path.relative(__dirname,thumbnailPath)
    media.status = MediaStatus.PUBLISHED

    const image = sharp(file.path);
    await Promise.all([
      image
        .resize({ width: 1920, height: 1080, fit: 'inside' })
        .toFile(largePath),
      image
        .resize({ width: 960, height: 540, fit: 'inside' })
        .toFile(mediumPath),
      image
        .resize({ width: 480, height: 270, fit: 'inside' })
        .toFile(smallPath),
      image
        .resize({ width: 240, height: 135, fit: 'inside' })
        .toFile(thumbnailPath),
    ]);

    const uploadPath = path.join(__dirname, '..', '..','..', 'uploads','media','orig', file.filename);
    fs.createReadStream(file.path).pipe(fs.createWriteStream(uploadPath));

    return this.mediaRepo.update(id,media);
  }

  async deleteMedia(id:number){
    return this.mediaRepo.delete(id)
  }

  async searchMedia(query: string){
    const medias = await this.mediaRepo
      .createQueryBuilder('media')
      .leftJoinAndSelect('media.posts', 'post')
      .where('media.fileName ILIKE :query', { query: `%${query}%` })
      .orderBy('media.createdAt', 'DESC')
      .getMany();

    return medias;
  }

  async findallbypaginate(limit: number, page: number){
    const [medias, count] = await this.mediaRepo.createQueryBuilder('media')
    .leftJoinAndSelect('media.posts', 'post')
    .take(limit)
    .skip((page - 1) * limit)
    .orderBy('media.createdAt', 'DESC')
    .getManyAndCount();
const totalPages = Math.ceil(count / limit);
return [medias, count,totalPages];
  }
}