import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Post } from 'src/shared/entity/post.entity';
import { User } from 'src/shared/entity/user.entity';
import { Vote } from 'src/shared/entity/vote.entity';
import { Repository } from 'typeorm';

@Injectable()
export class VoteService {
    constructor(
        @InjectRepository(Vote)
        private voteRepo: Repository <Vote>,
        @InjectRepository(Post)
        private postRepo: Repository <Post>,
        @InjectRepository(User)
        private userRepo: Repository <User>
    ){}

    async createVote(postId: number){
        const post = await this.postRepo.findOne({where:{id:postId}})
        const vote = new Vote();
        vote.post = post
        await this.voteRepo.save(vote)
      }

      async getVote(){
        return this.voteRepo.createQueryBuilder('vote')
                .leftJoinAndSelect('vote.post', 'post')
                .getMany()
      }

    async upVote(postId: number,userId:number){
        const post = await this.postRepo.findOne({where:{id:postId}})
        const user = await this.userRepo.findOne({where:{id:userId}})
        if(post){
            const existingVote = await this.voteRepo.createQueryBuilder('vote')
                .leftJoin('vote.post', 'post')
                .leftJoin('vote.user', 'user')
                .where('post.id = :postId', { postId})
                .where('user.id = :userId', { userId})
                .getOne()
            if(!existingVote){
                const vote = new Vote()
                vote.post = post
                vote.user = user
                vote.type = 1
                await this.voteRepo.save(vote)
                post.upvotes++
            }
            
                if(existingVote.type == 1){
                    post.upvotes--
                    existingVote.type = 0
                }
                else if(existingVote.type ==0){
                    post.upvotes++
                    existingVote.type = 1
                }
                else{
                    post.upvotes++
                    post.downvotes--
                    existingVote.type = 1
                }
                await this.voteRepo.save(existingVote)
                await this.postRepo.update(post.id,post);
            
        }
        else {
            throw new NotFoundException('Post not found');
        }
    }

    async downVote(postId: number,userId:number){
        const post = await this.postRepo.findOne({where:{id:postId}})
        const user = await this.userRepo.findOne({where:{id:userId}})
        if(post){
            const existingVote = await this.voteRepo.createQueryBuilder('vote')
                .leftJoin('vote.post', 'post')
                .leftJoin('vote.user', 'user')
                .where('post.id = :postId', { postId})
                .where('user.id = :userId', { userId})
                .getOne()
            if(!existingVote){
                const vote = new Vote()
                vote.post = post
                vote.user = user
                vote.type = -1
                await this.voteRepo.save(vote)
                post.downvotes++
            }
            
                if(existingVote.type == -1){
                    post.downvotes--
                    existingVote.type = 0
                }
                if(existingVote.type ==0){
                    post.downvotes++
                    existingVote.type = -1
                }
                else{
                    post.upvotes--
                    post.downvotes++
                    existingVote.type = -1
                }
                await this.voteRepo.save(existingVote)
                    await this.postRepo.update(post.id,post);
            
        }
        else {
            throw new NotFoundException('Post not found');
        }
    }
}
