import { Controller, Post, UseGuards, Get, Param,Req } from '@nestjs/common';
import { VoteService } from './vote.service';
import { jwtAuthGuard } from 'src/guard/jwt/jwt.guard';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { EmailConfirmationGuard } from 'src/guard/jwt/emailActive.guard';

@UseGuards(jwtAuthGuard,EmailConfirmationGuard)
@ApiTags('vote')
@ApiBearerAuth()
@Controller('vote')
export class VoteController {
    constructor(
        private voteService: VoteService
    ){}

    @Post(':id/up-vote')
    upVote(@Param('id') postId: string,@Req() req){
        return this.voteService.upVote(parseInt(postId),req.user.id)
    }

    @Post(':id/down-vote')
    downVote(@Param('id') postId: string,@Req() req){
        return this.voteService.downVote(parseInt(postId),req.user.id)
    }

    @Get()
    getVote(){
        return this.voteService.getVote()
    }

    @Post(':postId')
    createVote(@Param('postId') postId:string){
        return this.voteService.createVote(parseInt(postId))
    }
}
