import { Module } from '@nestjs/common';
import { VoteController } from './vote.controller';
import { VoteService } from './vote.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Vote } from 'src/shared/entity/vote.entity';
import { Post } from 'src/shared/entity/post.entity';
import { User } from 'src/shared/entity/user.entity';

@Module({
  imports:[TypeOrmModule.forFeature([Vote,Post,User])],
  controllers: [VoteController],
  providers: [VoteService],
  exports:[VoteService]
})
export class VoteModule {}
