import { Module } from '@nestjs/common';
import { EmailActivedController } from './email-actived.controller';
import { EmailActivedService } from './email-actived.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from 'src/shared/entity/user.entity';
import { JwtModule } from '@nestjs/jwt';
import { MailService } from 'src/shared/utilf/mailer/mail.service';

@Module({
  imports:[TypeOrmModule.forFeature([User]),
  JwtModule.register({
  })],
  controllers: [EmailActivedController],
  providers: [EmailActivedService,MailService]
})
export class EmailActivedModule {}
