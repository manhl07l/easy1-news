import { Body, Controller, Post, Req, UseGuards } from '@nestjs/common';
import { EmailActivedService } from './email-actived.service';
import { activedEmailDto } from 'src/shared/dto/activedEmail.dto';
import { jwtAuthGuard } from 'src/guard/jwt/jwt.guard';

@Controller('email-actived')
export class EmailActivedController {
    constructor(
        private emailActivedService: EmailActivedService
    ){}

    @Post('confirm')
    async confirm(@Body() confirmationData: activedEmailDto) {
        const email = await this.emailActivedService.decodeConfirmationToken(confirmationData.token);
        await this.emailActivedService.activeEmail(email);
    }

    @Post('resend-mail')
    @UseGuards(jwtAuthGuard)
    async resendEmail(@Req() req){
        await this.emailActivedService.resendEmail(req.user.id)
    }
}
