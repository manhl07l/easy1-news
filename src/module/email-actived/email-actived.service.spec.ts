import { Test, TestingModule } from '@nestjs/testing';
import { EmailActivedService } from './email-actived.service';

describe('EmailActivedService', () => {
  let service: EmailActivedService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [EmailActivedService],
    }).compile();

    service = module.get<EmailActivedService>(EmailActivedService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
