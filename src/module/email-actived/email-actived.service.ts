import { BadRequestException, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/shared/entity/user.entity';
import { MailService } from 'src/shared/utilf/mailer/mail.service';
import { Repository } from 'typeorm';

@Injectable()
export class EmailActivedService {
    constructor(
        @InjectRepository(User)
        private userRepo: Repository<User>,
        private readonly jwtService: JwtService,
        private readonly mailService: MailService
    ){}

    async activeEmail(email: string){
        const user = await this.userRepo.findOne({where:{email}})
        if (user.isActivated) {
            throw new BadRequestException('Email already actived');
        }
        await this.userRepo.update({email},{isActivated:true})
    }

    public async decodeConfirmationToken(token: string) {
        try {
          const payload = await this.jwtService.verify(token, {
            secret: process.env.JWT_VERIFICATION_SECRET,
          });
     
          if (typeof payload === 'object' && 'email' in payload) {
            return payload.email;
          }
          throw new BadRequestException();
        } catch (error) {
          if (error?.name === 'TokenExpiredError') {
            throw new BadRequestException('Email confirmation token expired');
          }
          throw new BadRequestException('Bad confirmation token');
        }
    }

    async resendEmail(id:number){
      const user = await this.userRepo.findOne({where:{id}})
      if (user.isActivated) {
          throw new BadRequestException('Email already actived');
      }
      await this.mailService.sendMailActived(user.email)
    }
}
