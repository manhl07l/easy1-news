import { Test, TestingModule } from '@nestjs/testing';
import { EmailActivedController } from './email-actived.controller';

describe('EmailActivedController', () => {
  let controller: EmailActivedController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [EmailActivedController],
    }).compile();

    controller = module.get<EmailActivedController>(EmailActivedController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
