import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm/dist';
import { User } from 'src/shared/entity/user.entity';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { JwtModule } from '@nestjs/jwt/dist';
import { JwtStrategy } from 'src/guard/jwt/jwt.stragety';
import { Codereset } from 'src/shared/entity/codereset.enity';
import { MailService } from 'src/shared/utilf/mailer/mail.service';
import { PassportModule } from '@nestjs/passport';
import { Role } from 'src/shared/entity/role.entity';
import { RefreshTokenStrategy } from 'src/guard/jwt/refreshJwt.stragety';

@Module({
  imports:[TypeOrmModule.forFeature([User,Codereset,Role]),
  PassportModule,
  JwtModule.register({})],
  controllers: [AuthController],
  providers: [AuthService,JwtStrategy,MailService,RefreshTokenStrategy]
})
export class AuthModule {}
