import { Body, ClassSerializerInterceptor, Controller, Post, Req, UseGuards, UseInterceptors } from '@nestjs/common';
import { loginDto, registerDto } from 'src/shared/dto/auth.dto';
import { AuthService } from './auth.service';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { jwtAuthGuard } from 'src/guard/jwt/jwt.guard';
import { refreshTokenGuard } from 'src/guard/jwt/refreshJwt.guard';
import { MailService } from 'src/shared/utilf/mailer/mail.service';

@ApiTags('Auth')
@Controller('auth')
export class AuthController {
    constructor(
        private readonly authService: AuthService,
        private readonly mailService: MailService
    ){}

    @UseInterceptors(ClassSerializerInterceptor)
    @Post('register')
    async register(@Body() register:registerDto){
        const user = await this.authService.register(register)
        await this.mailService.sendMailActived(register.email)
        return user
    }

    @UseInterceptors(ClassSerializerInterceptor)
    @Post('register-author')
    async registerAuthor(@Body() register:registerDto){
        const user = await this.authService.registerAuthor(register)
        return user
    }

    @Post('login')
    login(@Body() login:loginDto){
        return this.authService.login(login)
    }

/*     @Post('forgot-password')
    forgotPassword(@Body() forgotPassword:ForgotPasswordDto){
        return this.authService.forgotPassword(forgotPassword)
    }

    @Post('reset-password')
    resetPassword(@Body() resetPassword:resetPasswordDto){
        return this.authService.resetPassword(resetPassword)
    } */
    @UseGuards(refreshTokenGuard)
    @ApiBearerAuth()
    @Post('refresh-token')
    async refreshToken(@Req() req){
        return this.authService.refreshToken(req.user.id,req.user.refreshToken)
    }

    @UseGuards(jwtAuthGuard)
    @ApiBearerAuth()
    @Post('logout')
    async logout(@Req() req){
        await this.authService.logout(req.user.id)
        return {message:'logout success'}
    }
}
