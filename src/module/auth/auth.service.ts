import { BadRequestException, ForbiddenException, Injectable, NotFoundException, UnauthorizedException } from '@nestjs/common';
import { User } from 'src/shared/entity/user.entity';
import { InjectRepository } from '@nestjs/typeorm/dist/common';
import { Repository } from 'typeorm';
import { registerDto, loginDto } from 'src/shared/dto/auth.dto';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { MailService } from 'src/shared/utilf/mailer/mail.service';
import { Codereset } from 'src/shared/entity/codereset.enity';
import { Role } from 'src/shared/entity/role.entity';

@Injectable()
export class AuthService {
    constructor(
        @InjectRepository(User)
        private readonly userRepo:Repository<User>,
        private readonly jwtService: JwtService,
        private readonly mailerService: MailService,
        @InjectRepository(Codereset)
        private readonly codeResetRepo: Repository<Codereset>,
        @InjectRepository(Role)
        private readonly roleRepo: Repository<Role>
    ){}

    async register(register:registerDto){
        const {email,username,password,confirmPassword} = register
        const users = await this.userRepo.findOne({where:{email}})
        const role = await this.roleRepo.findOne({where:{title:4}})
        if(users){
            throw new BadRequestException('email existed');
        }
        if(password != confirmPassword){
          throw new BadRequestException('comfirm password incorrect')
        }
        const hashPassword=await bcrypt.hash(password,10)
        const user = new User()
        user.email = email
        user.username = username
        user.password = hashPassword
        user.role =role
        return this.userRepo.save(user)
    }

    async registerAuthor(register:registerDto){
      const {email,username,password,confirmPassword} = register
        const users = await this.userRepo.findOne({where:{email}})
        const role = await this.roleRepo.findOne({where:{title:3}})
        if(users){
            throw new BadRequestException('email existed');
        }
        if(password != confirmPassword){
          throw new BadRequestException('comfirm password incorrect')
        }
        const hashPassword=await bcrypt.hash(password,10)
        const user = new User()
        user.email = email
        user.username = username
        user.password = hashPassword
        user.role =role
        user.isActivated=true
        return this.userRepo.save(user)
    }

    async login(signin:loginDto){
        const {email,password}=signin
        const user = await this.userRepo.findOne({where:{email}})
        if(!user){
            throw new BadRequestException('email or password incorrect')
        }
        const checkpassword = await bcrypt.compare(password,user.password)
        if(!checkpassword){
          throw new BadRequestException('email or password incorrect')
        }
        const jwt = this.createToken(user.id)
        const refreshToken = this.createRefreshToken(user.id)
        await this.userRepo.update(user.id,{refreshToken:refreshToken})
        return {jwt,refreshToken}
    }

/*     async forgotPassword(ForgotPassword:ForgotPasswordDto){
        const user =await this.userRepo.findOne({where:{email:ForgotPassword.email}})
        if(!user){
            throw new BadRequestException('Email incorrect')
        }
        const resetPasswordToken = await this.generateResetPasswordToken(user);
        await this.mailerService.sendMailResetPassword(ForgotPassword.email,resetPasswordToken)
        return 'mail was send'
    }

    async resetPassword(resetPassword:resetPasswordDto){
        const codereset = await this.validateUser(resetPassword.code)
        if(!codereset){
            throw new BadRequestException('Invalidate token')
        }
        const newPassword =await bcrypt.hash(resetPassword.newPassword,10)
        const user = await this.userRepo.findOne({where:{id:codereset.id}})
        await this.userRepo.update(user.id,{password:newPassword})
    } */

    private createToken(id:number){
        return this.jwtService.sign({id},{secret:process.env.JWT_SECRET,expiresIn: '15m'})
    }

    private createRefreshToken(id:number){
      return this.jwtService.sign({id},{secret:process.env.JWT_REFRESH_SECRET,expiresIn: '30d'})
    }

    async refreshToken(id: number,refreshToken:string){
      const user = await this.userRepo.findOne({where:{id}})
      if (!user || !user.refreshToken)
        throw new ForbiddenException('Access Denied')
      if (refreshToken != user.refreshToken)
        throw new ForbiddenException('Access Denied')
      const jwt = this.createToken(id)
      return {jwt}
    }

    async validateUser(id:number){
      const user = await this.userRepo.createQueryBuilder('user')
      .leftJoinAndSelect('user.role','role')
      .where('user.id=:id',{id})
      .getOne()
        if(!user){
            throw new Error('user not found')
        }
        return user
    }

    async logout(id:number){
      await this.userRepo.update(id, { refreshToken: null })
    }

/*
    async generateResetPasswordToken(user: User): Promise<string> {
        const userId =user.id
        const random =crypto.randomBytes(6).toString('hex').toUpperCase()
        const code = await bcrypt.hash(random,10)
        const resetCode = this.codeResetRepo.create({userId,code});
        await this.codeResetRepo.save(resetCode)
        const payload: JwtPayload = {
          sub: user.id,
          token: random,
          iat: Date.now(),
          exp: Date.now() + 3600000, // 1 hour expiration
        };
        return this.jwtService.sign(payload, {
          secret: process.env.JWT_RESET_PASSWORD_SECRET,
        });
      } */
}
