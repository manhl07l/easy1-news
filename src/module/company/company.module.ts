import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm/dist';
import { Company } from 'src/shared/entity/company.entity';
import { CompanyController } from './company.controller';
import { CompanyService } from './company.service';
import { User } from 'src/shared/entity/user.entity';
import { Post } from 'src/shared/entity/post.entity';

@Module({
  imports:[TypeOrmModule.forFeature([Company,User,Post])],
  controllers: [CompanyController],
  providers: [CompanyService]
})
export class CompanyModule {}
