import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Company } from 'src/shared/entity/company.entity';
import { User } from 'src/shared/entity/user.entity';
import { Repository } from 'typeorm';

@Injectable()
export class CompanyService {
    constructor(
        @InjectRepository(Company)
        private readonly companyRepo: Repository<Company>,
        @InjectRepository(User)
        private readonly userRepo: Repository<User>
    ){}

    async getAllCompany(){
        return this.companyRepo.createQueryBuilder('company')
            .leftJoinAndSelect('company.posts', 'post')
            .getMany()
    }

    async addCompanyToUser(userId: number,companyId: number){
        const user = await this.userRepo.findOne({where:{id:userId}})
        const company = await this.companyRepo.findOne({where:{id:companyId}})
        user.company = company
        return this.userRepo.update(userId,user)
    }

}
