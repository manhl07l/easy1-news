import { Controller,Get,Post,Param,Body, UseGuards } from '@nestjs/common';
import { jwtAuthGuard } from 'src/guard/jwt/jwt.guard';
import { Roles } from 'src/guard/role/role.decorations';
import { RoleGuard } from 'src/guard/role/role.guard';
import { RoleName } from 'src/shared/entity/role.entity';
import { CompanyService } from './company.service';

@Controller('company')
export class CompanyController {
    constructor(
        private companyService: CompanyService
    ){}

    @Get()
    /* @UseGuards(jwtAuthGuard,RoleGuard)
    @Roles(RoleName.ADMIN,RoleName.MANAGER) */
    getAllCompany(){
        return this.companyService.getAllCompany()
    }

    /* @Post('/:id')
    @UseGuards(jwtAuthGuard,RoleGuard)
    @Roles(RoleName.ADMIN,RoleName.MANAGER)
    addCompanyToUser(@Param('id') userId:string, @Body() companyId:number){
        return this.companyService.addCompanyToUser(parseInt(userId),companyId)
    } */
}
