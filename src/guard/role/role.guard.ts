import { Injectable } from "@nestjs/common/decorators";
import { CanActivate } from "@nestjs/common/interfaces";
import { Reflector } from "@nestjs/core";
import { ExecutionContext } from "@nestjs/common";
import { RoleName } from "src/shared/entity/role.entity";

@Injectable()
export class RoleGuard implements CanActivate {
  constructor(private readonly reflector: Reflector) {}

  canActivate(context: ExecutionContext) {
    const requiredRoles = this.reflector.getAllAndOverride<RoleName[]>('roles', [
      context.getHandler(),
      context.getClass(),
    ]);

    if (!requiredRoles) {
      return true;
    }

    const request = context.switchToHttp().getRequest();
    const userRole = request.user?.role?.title;
    console.log(request.user)
    return requiredRoles.includes(userRole);
  }
}