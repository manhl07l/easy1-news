import { RoleName } from "src/shared/entity/role.entity";
import { SetMetadata } from "@nestjs/common";

export const Roles = (...role: RoleName[]) => SetMetadata('roles', role);