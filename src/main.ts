import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { ValidationPipe } from '@nestjs/common';
import * as passport from 'passport';
import { JwtStrategy } from './guard/jwt/jwt.stragety';
import * as admin from 'firebase-admin';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(new ValidationPipe({ transform: true }));
  passport.use(app.get(JwtStrategy));
  app.use(passport.initialize());
  const config = new DocumentBuilder()
    .setTitle('Cats example')
    .setDescription('The cats API description')
    .setVersion('1.0')
    .addTag('cats')
    .addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);
  app.enableCors({
    origin: '*',
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS',
    credentials: true,
  });
  var servieAcount = require('../firebase-noti.json')
  admin.initializeApp({
    credential: admin.credential.cert(servieAcount),
    databaseURL: "https://easy1-news-default-rtdb.asia-southeast1.firebasedatabase.app"
  });
  await app.listen(3000);
}

bootstrap();
