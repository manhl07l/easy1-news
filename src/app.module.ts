import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config/dist';
import { AuthModule } from './module/auth/auth.module';
import { PostModule } from './module/post/post.module';
import { CategoryModule } from './module/category/category.module';
import { UserModule } from './module/user/user.module';
import { MediaModule } from './module/media/media.module';
import { CompanyModule } from './module/company/company.module';
import { TypeOrmModule } from '@nestjs/typeorm/dist';
import { RoleGuard } from './guard/role/role.guard';
import { jwtAuthGuard } from './guard/jwt/jwt.guard';
import { MailModule } from './shared/utilf/mailer/mail.module';
import { User } from './shared/entity/user.entity';
import { Category } from './shared/entity/category.entity';
import { Post } from './shared/entity/post.entity';
import { Media } from './shared/entity/media.entity';
import { Company } from './shared/entity/company.entity';
import { Role } from './shared/entity/role.entity';
import { SeedModule } from './shared/seed/seed.module';
import { CommentModule } from './module/comment/comment.module';
import { Comment } from './shared/entity/comment.enity';
import { NotificationModule } from './module/notification/notification.module';
import { MeiliSearchModule } from 'nestjs-meilisearch';
import { Vote } from './shared/entity/vote.entity';
import { VoteModule } from './module/vote/vote.module';
import { MulterModule } from '@nestjs/platform-express';
import { EmailActivedModule } from './module/email-actived/email-actived.module';
import { refreshTokenGuard } from './guard/jwt/refreshJwt.guard';

@Module({
  imports: [
    ConfigModule.forRoot(),
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: process.env.DB_HOST,
      port: 5432,
      username: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_NAME,
      entities: [User,Category,Post,Media,Company,Role,Comment,Vote],
      synchronize: true,
    }),
    MeiliSearchModule.forRoot({
      host: 'http://localhost:8000',
      apiKey: 'masterKey',
    }),
    MulterModule.register({
      dest: './uploads', // Set the destination directory for uploaded files
    }),
    AuthModule,
    PostModule,
    CategoryModule,
    UserModule,
    MediaModule,
    CompanyModule,
    MailModule,
    SeedModule,
    CommentModule,
    NotificationModule,
    VoteModule,
    EmailActivedModule,
  ],
  controllers: [],
  providers: [RoleGuard,jwtAuthGuard,refreshTokenGuard],
})
export class AppModule {
}
