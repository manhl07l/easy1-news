import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Role } from '../entity/role.entity';
import { User } from '../entity/user.entity';
import { SeedService } from './seed.service';
import { Company } from '../entity/company.entity';

@Module({
    imports: [TypeOrmModule.forFeature([Role,User,Company])],
    providers: [SeedService]
  })
  export class SeedModule {
    constructor(private readonly seedService: SeedService) {}
  
    async onApplicationBootstrap(): Promise<void> {
        await this.seedService.admin()
        await this.seedService.Company()
        await this.seedService.role()
    }
  }