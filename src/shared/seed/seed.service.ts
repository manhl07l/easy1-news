import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Role, RoleName } from '../entity/role.entity';
import { User } from '../entity/user.entity';
import * as bcrypt from 'bcrypt'
import { Company } from '../entity/company.entity';

@Injectable()
export class SeedService {
    constructor(
        @InjectRepository(Role)
        private roleRepo: Repository<Role>,
        @InjectRepository(User)
        private userRepo: Repository<User>,
        @InjectRepository(Company)
        private companyRepo: Repository<Company>,
    ){}

    async Company(){
        const Company = [{
            id:1,
            title:"EasyWater",
            description:"EasyWater"
        },
        {
            id:2,
            title:"EasyPurchase",
            description:"EasyPurchase"
        },
        {
            id:3,
            title:"EasyTech",
            description:"EasyTech"
        }
        ]
        const company = this.companyRepo.create(Company)
        await this.companyRepo.save(company)
    }

    async role(){
        const createrole= [
            {
                id:1,
                title: RoleName.ADMIN,
                description: 'All Access'
            },
            {
                id:2,
                title: RoleName.MANAGER,
                description: 'Add user to company'
            },
            {
                id:3,
                title: RoleName.AUTHOR,
                description: 'Write post'
            },
            {
                id:4,
                title: RoleName.USER,
                description: 'Read post'
            }
        ]
            const roleenity = this.roleRepo.create(createrole)
            await this.roleRepo.save(roleenity)
    }

    async admin(){
        const adminrole = await this.roleRepo.findOne({where:{title:RoleName.ADMIN}})
        const Admin = {
            id:1,
            email: "admin@admin.com",
            username: 'admin',
            password: await bcrypt.hash("admin123",10),
            isActivated: true,
            role:adminrole
        }
        const admin = this.userRepo.create(Admin)
        await this.userRepo.save(admin)
    }
}