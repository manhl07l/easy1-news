import { Injectable } from "@nestjs/common";
import { MailerService } from '@nestjs-modules/mailer';
import { JwtService } from "@nestjs/jwt";
import VerificationTokenPayload from "./verificationTokenPayload.interface";

@Injectable()
export class MailService{
    constructor(
        private readonly mailerService: MailerService,
        private readonly jwtService: JwtService
    ){}

    async sendMailActived(email:string){
        const subject = 'Email Active'
        const payload: VerificationTokenPayload = { email };
        const token = this.jwtService.sign(payload, {
            secret: process.env.JWT_VERIFICATION_SECRET,
            expiresIn: `3600s`
        });
 
        const url = `${process.env.URL}?token=${token}`;
 
        const text = `Welcome to the application. To confirm the email address, click here: ${url}`;
        await this.sendmail(email,subject,text)
    }

    private sendmail(to: string,subject:string,text:string){
        const mailOptions={
            from: process.env.SMTP_FROM,
            to: to,
            subject: subject,
            text: text,
        }
        return this.mailerService.sendMail(mailOptions)
    }
}