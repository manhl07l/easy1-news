import { Module } from "@nestjs/common";
import { MailService } from "./mail.service";
import { MailerModule } from "@nestjs-modules/mailer";
import { JwtModule } from "@nestjs/jwt";

@Module({
    imports:[MailerModule.forRoot({
        transport: {
            host: process.env.SMTP_HOST,
            port: parseInt(process.env.SMTP_PORT),
            auth: {
                user: process.env.SMTP_USERNAME,
                pass: process.env.SMTP_PASSWORD,
              },
        }
    }),
    JwtModule.register({})
    ],
    providers:[MailService],
    exports:[MailService]
})
export class MailModule{
}
