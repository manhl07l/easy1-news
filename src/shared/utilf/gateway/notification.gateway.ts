import {
    WebSocketGateway,
    WebSocketServer,
    OnGatewayConnection,
    OnGatewayDisconnect,
  } from '@nestjs/websockets';
  import { Server, Socket } from 'socket.io';
  
  @WebSocketGateway()
  export class notificationGateway
    implements OnGatewayConnection, OnGatewayDisconnect
  {
    @WebSocketServer()
    server: Server;
  
    users: { [companyId: number]: string[] } = {}; // companyId to socketIds mapping
  
    handleConnection(client: any) {
      console.log(`Client connected: ${client.id}`);
    }
  
    handleDisconnect(client: any) {
      console.log(`Client disconnected: ${client.id}`);
    }
  
    joinCompanyRoom(client: Socket, companyId: number) {
        const socketIds = this.users[companyId] || [];
        socketIds.push(client.id);
        this.users[companyId] = socketIds;
    
        client.join(`company_${companyId}`);
    }
  
    sendNotificationToCompany(companyId: number, notification: any) {
      const socketIds = this.users[companyId];
      if (socketIds) {
        for (const socketId of socketIds) {
          this.server.to(`${socketId}`).emit('notification', notification);
        }
      }
    }
  }
  