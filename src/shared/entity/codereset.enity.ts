import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Codereset{
    @PrimaryGeneratedColumn()
    userId:number

    @Column()
    code: string
}