import { Entity, Column, PrimaryGeneratedColumn, OneToMany, CreateDateColumn, UpdateDateColumn, ManyToMany, JoinTable } from 'typeorm';
import { User } from './user.entity';
import { Post } from './post.entity';

@Entity()
export class Company {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  title: string;

  @Column({ nullable: true})
  description: string;

  @ManyToMany(()=>Post, (post)=> post.companies,{nullable:true,onDelete:"CASCADE"})
  @JoinTable()
  posts: Post[]

  @OneToMany(()=>User, (user)=> user.company,{nullable:true})
  users: User[]

  @CreateDateColumn()
  createdAt: Date

  @UpdateDateColumn()
  updatedAt: Date
}
