import { Entity,Column,PrimaryGeneratedColumn, ManyToOne, OneToMany, UpdateDateColumn, JoinTable } from "typeorm";
import { CreateDateColumn } from "typeorm/decorator/columns/CreateDateColumn";
import { Exclude } from "class-transformer";
import { Post } from "./post.entity";
import { Min} from "class-validator";
import { Media } from "./media.entity";
import { Role } from "./role.entity";
import { Comment } from "./comment.enity";
import { Vote } from "./vote.entity";
import { Company } from "./company.entity";

@Entity()
export class User{
    @PrimaryGeneratedColumn()
    id:number

    @Column({ unique: true })
    email:string

    @Column({ unique: true })
    username:string

    @Column()
    @Exclude()
    @Min(6)
    password:string

    @Column({ type: 'boolean', default: false })
    isActivated: boolean;  

    @ManyToOne(()=>Role, (role) => role.users,{onDelete:"NO ACTION"})
    @JoinTable()
    role: Role

    @OneToMany(()=>Media, (media)=>media.uploadedBy,{nullable:true,onDelete:"CASCADE"})
    medias: Media[]

    @OneToMany(()=>Post, (post) => post.author,{nullable:true,onDelete:"CASCADE"})
    posts: Post[]

    @ManyToOne(()=>Company, (company)=> company.users,{nullable:true})
    company: Company

    @OneToMany(()=>Comment, (comment) => comment.user,{nullable:true,onDelete:"CASCADE"})
    comment: Comment[]

    @OneToMany(()=>Vote, vote=>vote.user,{onDelete:"CASCADE"})
    votes:Vote[]

    @Column({nullable:true})
    refreshToken: string

    @CreateDateColumn()
    createAt: Date

    @UpdateDateColumn()
    updateAt: Date
}
