import { Entity,Column,PrimaryGeneratedColumn, ManyToMany,CreateDateColumn,UpdateDateColumn, ManyToOne, JoinTable } from "typeorm";
import { User } from "./user.entity";
import { Post } from "./post.entity";

@Entity()
export class Comment{
    @PrimaryGeneratedColumn()
    id: number

    @Column()
    content:string

    @CreateDateColumn()
    createdAt: Date;
  
    @UpdateDateColumn()
    updatedAt: Date;

    @ManyToOne(() => User, (user) => user.comment)
    @JoinTable()
    user: User

    @ManyToOne(()=>Post, post => post.comments)
    @JoinTable()
    post: Post
}