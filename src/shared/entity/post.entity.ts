import { Entity,Column,PrimaryGeneratedColumn, ManyToMany,ManyToOne, UpdateDateColumn, OneToMany, BeforeUpdate, BeforeInsert, JoinTable } from "typeorm";
import { CreateDateColumn } from "typeorm/decorator/columns/CreateDateColumn";
import { Category } from "./category.entity";
import { Media } from "./media.entity";
import { User } from "./user.entity";
import { Comment } from "./comment.enity";
import { Vote } from "./vote.entity";
import { Company } from "./company.entity";

export enum PostStatus{
    PUBLIC = 1,
    HIDDEN = 0
}

@Entity()
export class Post {
    @PrimaryGeneratedColumn()
    id:number

    @Column()
    title:string

    @Column({ unique: true })
    slug: string;

    @Column({ nullable: true })
    description:string

    @Column()
    content: string

    @Column({default: PostStatus.PUBLIC})
    status: PostStatus

    @Column({nullable:true})
    avatar:string

    @Column({default:0})
    view:number

    @ManyToMany(()=>Category,(category) => category.posts,{onDelete:"CASCADE"})
    @JoinTable()
    categories: Category[]
    
    @ManyToMany(()=>Company, (company)=>company.posts,{onDelete:"CASCADE"})
    @JoinTable()
    companies: Company[]

    @ManyToOne(() => User, (user) => user.posts)
    @JoinTable()
    author: User

    @ManyToOne(()=>Media, (media)=>media.posts)
    @JoinTable()
    medias: Media

    @OneToMany(()=>Comment, (comment) => comment.post,{nullable:true,onDelete:"CASCADE"})
    comments: Comment[]

    @OneToMany(()=>Vote, Vote=>Vote.post,{onDelete:"CASCADE"})
    @JoinTable()
    votes:Vote[]

    @Column({default:0})
    upvotes: number

    @Column({default:0})
    downvotes: number

    @CreateDateColumn()
    createdAt:Date

    @UpdateDateColumn()
    updatedAt:Date
}
