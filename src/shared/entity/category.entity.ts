import { Entity,Column,PrimaryGeneratedColumn, ManyToMany,CreateDateColumn,UpdateDateColumn, JoinTable } from "typeorm";
import { Post } from "./post.entity";

@Entity()
export class Category {
    @PrimaryGeneratedColumn()
    id: number

    @Column()
    title:string

    @Column({nullable:true})
    description:string

    @ManyToMany(()=>Post,(post) => post.categories,{nullable:true,onDelete:"CASCADE"})
    @JoinTable()
    posts: Post[]

    @Column({nullable:true})
    postCount: number

    @CreateDateColumn()
    createdAt: Date;
  
    @UpdateDateColumn()
    updatedAt: Date;
  
}
