import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, CreateDateColumn, UpdateDateColumn, OneToMany, JoinTable } from 'typeorm';
import { Post } from './post.entity';
  import { User } from './user.entity';
  
  export enum MediaStatus {
    PUBLISHED = 0,
    DELETED = -1,
  }
  
  @Entity()
  export class Media {
    @PrimaryGeneratedColumn()
    id: number;
  
    @Column({ unique: true })
    fileName: string;
  
    @Column()
    totalSize: number;
  
    @Column({type: 'enum', enumName: 'mediaStatus', enum: MediaStatus, default: MediaStatus.PUBLISHED})
    status: MediaStatus;
  
    @Column({ unique: true })
    largePath: string;
  
    @Column({ unique: true })
    smallPath: string;
  
    @Column()
    mediumPath: string;
  
    @Column({ unique: true })
    thumbnailPath: string;

    @OneToMany(()=>Post, (post)=>post.medias,{nullable:true,onDelete:"CASCADE"})
    @JoinTable()
    posts: Post[]

    @ManyToOne(() => User, (user) => user.medias)
    @JoinTable()
    uploadedBy: User;
  
    @CreateDateColumn()
    createdAt: Date;
  
    @UpdateDateColumn()
    updatedAt: Date;
  }
  
