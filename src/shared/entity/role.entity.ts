import { Entity,Column,PrimaryGeneratedColumn, OneToMany } from "typeorm";
import { User } from "./user.entity";

export enum RoleName{
    ADMIN = 1,
    MANAGER = 2,
    AUTHOR = 3,
    USER = 4
}

@Entity()
export class Role{
    @PrimaryGeneratedColumn()
    id:number

    @Column({default: RoleName.USER})
    title: RoleName

    @Column({nullable:true})
    description:string

    @OneToMany(()=>User, (user) => user.role,{nullable:true})
    users: User[]
}
