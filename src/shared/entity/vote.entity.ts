import { Entity,Column,PrimaryGeneratedColumn, ManyToOne, CreateDateColumn, UpdateDateColumn } from "typeorm";
import { User } from "./user.entity";
import { Post } from "./post.entity";

export enum type{
    DOWN = -1,
    NONE = 0,
    UP = 1
}

@Entity()
export class Vote{
    @PrimaryGeneratedColumn()
    id:number

    @Column({default: type.NONE})
    type: type

    @ManyToOne(() => User, user => user.votes,{onDelete:"CASCADE"})
    user: User;

    @ManyToOne(() => Post, post => post.votes,{onDelete:"CASCADE"})
    post: Post;

    @CreateDateColumn()
    createdAt: Date

    @UpdateDateColumn()
    updateAt: Date
}