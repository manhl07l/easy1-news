import { IsNotEmpty, IsNumber } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";

export class updateVoteDto{
    @ApiProperty()
    @IsNotEmpty()
    @IsNumber()
    postId: number

    @ApiProperty()
    @IsNotEmpty()
    @IsNumber()
    userId:number
}