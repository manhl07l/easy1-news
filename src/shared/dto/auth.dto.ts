import { IsString,IsEmail, IsNotEmpty, MinLength, MaxLength } from "class-validator";
import { ApiProperty } from '@nestjs/swagger';

export class registerDto{
    @ApiProperty()
    @IsNotEmpty()
    @IsEmail()
    email:string

    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    username:string

    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    password:string

    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    confirmPassword:string
}

export class loginDto{
    @ApiProperty()
    @IsNotEmpty()
    @IsEmail()
    email:string

    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    @IsNotEmpty()
    password:string
}

export class ForgotPasswordDto{
    @IsNotEmpty()
    @IsString()
    email:string
}

export class resetPasswordDto{
    @IsNotEmpty()
    @IsString()
    code:string

    @IsNotEmpty()
    @IsString()
    @MinLength(6)
    @MaxLength(32)
    newPassword:string
}