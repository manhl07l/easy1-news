import { IsNotEmpty, IsOptional, IsString, IsEnum, IsArray, IsNumber, isNumber, IsDate } from 'class-validator';
import { PostStatus } from '../entity/post.entity';
import { ApiProperty } from '@nestjs/swagger';

export class getPostDto {
  @ApiProperty({required:false})
  @IsOptional()
  @IsNumber()
  companyId: number

  @ApiProperty({required:false})
  @IsOptional()
  @IsNumber()
  categoryId: number

  @ApiProperty({required:false})
  @IsOptional()
  @IsDate()
  date: Date

  @ApiProperty({required:false})
  @IsOptional()
  @IsString()
  query: string

  @ApiProperty()
  @IsOptional()
  @IsNumber()
  page: number=1;

  @ApiProperty()
  @IsOptional()
  @IsNumber()
  limit: number=10;
}

export class CreatePostDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  title: string;

  @ApiProperty()
  @IsOptional()
  @IsString()
  description: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  content: string;

  @ApiProperty()
  @IsOptional()
  @IsArray()
  categoryIds: number[];

  @ApiProperty()
  @IsOptional()
  @IsArray()
  companiesIds: number[];
  
}


export class UpdatePostDto {
  @ApiProperty()
  @IsOptional()
  @IsString()
  title: string;

  @ApiProperty()
  @IsOptional()
  @IsString()
  description: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  content: string;

  @ApiProperty()
  @IsOptional()
  @IsArray()
  categoryIds: number[];
  
  @ApiProperty()
  @IsOptional()
  @IsEnum(PostStatus)
  status: PostStatus;

  @ApiProperty()
  @IsOptional()
  @IsArray()
  companiesIds: number[];
}
