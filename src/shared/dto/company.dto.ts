import { IsNotEmpty, IsOptional, IsString, IsArray } from 'class-validator';

export class CreateCompanyDto {
  @IsNotEmpty()
  @IsString()
  title: string;

  @IsOptional()
  @IsString()
  desctiption: string;

  @IsOptional()
  @IsArray()
  userIds: string[];
}

export class UpdateCompanyDto {
  @IsOptional()
  @IsString()
  title: string;

  @IsOptional()
  @IsString()
  desctiption: string;

  @IsOptional()
  @IsArray()
  userIds: string[];
}