import { IsString, IsNotEmpty } from 'class-validator';
 
export class activedEmailDto {
  @IsString()
  @IsNotEmpty()
  token: string;
}