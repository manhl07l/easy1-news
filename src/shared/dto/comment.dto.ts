import { IsNotEmpty, IsString } from "class-validator";
import { ApiProperty } from '@nestjs/swagger';

export class createCommentDto{
    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    content:string
}

export class updateCommentDto{
    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    content:string
}