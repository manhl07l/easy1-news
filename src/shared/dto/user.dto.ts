import { IsNotEmpty, IsOptional, IsString, IsEmail, IsArray, MinLength, MaxLength, IsBoolean } from 'class-validator';
import { RoleName } from '../entity/role.entity';

export class CreateUserDto {
  @IsNotEmpty()
  @IsString()
  @MinLength(4)
  @MaxLength(20)
  username: string;

  @IsNotEmpty()
  @IsEmail()
  email: string;

  @IsNotEmpty()
  @IsString()
  @MinLength(6)
  @MaxLength(32)
  password: string;

  @IsNotEmpty()
  @IsBoolean()
  isActivated: boolean;

  @IsOptional()
  @IsArray()
  companyName: string;

  @IsOptional()
  roleName: RoleName
}

export class UpdateUserDto {
  @IsOptional()
  @IsString()
  @MinLength(4)
  @MaxLength(20)
  username: string;

  @IsOptional()
  @IsEmail()
  email: string;

  @IsOptional()
  @IsString()
  @MinLength(6)
  @MaxLength(32)
  password: string;

  @IsOptional()
  @IsBoolean()
  isActivated: boolean;

  @IsOptional()
  companyName: string;

  @IsOptional()
  roleName: RoleName
}

export class changePasswordDto{
  @IsString()
  @IsNotEmpty()
  currentPassword: string

  @IsString()
  @IsNotEmpty()
  newPassword: string

  @IsString()
  @IsNotEmpty()
  confirmPassword: string
}
